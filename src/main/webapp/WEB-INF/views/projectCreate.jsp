<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>ADD NEW PROJECT</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a style="font-size: 26px" class="navbar-brand mb-0 h1" href="/">PROJECT_MANAGER</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div style="margin-left: 30px" class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a style="font-size: 20px" class="nav-link" href="/">HOME</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 20px; margin-left: 10px" class="nav-link" href="/projects">PROJECTS</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 20px; margin-left: 10px" class="nav-link" href="/tasks">TASKS</a>
            </li>
        </ul>
    </div>
</nav>
<h1 style="text-align: center; margin-top: 50px">ADD NEW PROJECT</h1>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
    <form style="margin-top: 20px; padding: 20px" action="/project-create " class="bg-secondary rounded" method="POST">
        <div class="form-group">
            <label for="name">Name of project</label>
            <input type="text" class="form-control text-light bg-dark" name="name" id="name">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control text-light bg-dark" name="description" id="description">
        </div>
<%--        <spring:select path="status" items="${statuses}"/>--%>
        <div class="form-group">
            <label for="endDate">Date of end</label>
            <input type="text" class="form-control text-light bg-dark"  name="endDate" id="endDate" placeholder="DD.MM.YYYY">
        </div>
        <div class="form-group">
            <label for="beginDate">Date of begin</label>
            <input type="text" class="form-control text-light bg-dark" name="beginDate" id="beginDate" placeholder="DD.MM.YYYY">
        </div>
        <button style="display: block; margin: 0 auto" type="submit" class="btn btn-success btn-lg">ADD NEW PROJECT</button>
    </form>
    </div>
    <div class="col-md-4"></div>
</div>
</body>
</html>