package ru.trifonov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.enumerate.CurrentStatus;
import ru.trifonov.tm.util.IdUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "projects")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractEntity {
    @Id
    @NotNull
    @Column (name = "id", unique = true)
    protected String id = IdUtil.getUUID();

    @NotNull
    @Column (name = "name", nullable = false)
    private String name;

    @NotNull
    @Column (name = "description", nullable = false)
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column (name = "status", nullable = false)
    private CurrentStatus status = CurrentStatus.PLANNED;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column (name = "begin_date", nullable = false)
    private Date beginDate = new Date();

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column (name = "end_date", nullable = false)
    private Date endDate = new Date();

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column (name = "create_date", nullable = false)
    private final Date createDate = new Date();

    @Nullable
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.ALL)
    private List<Task> tasks;

    public Project(
            @NotNull String name, @NotNull String description,
            @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.name = name;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public Project(
            @NotNull String id, @NotNull String name,
            @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  STATUS " + status +
                "  PROJECT CREATE DATE " + dateFormat.format(createDate) +
                "  PROJECT BEGIN DATE " + dateFormat.format(beginDate) +
                "  PROJECT END DATE " + dateFormat.format(endDate);
    }
}
